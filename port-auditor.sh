while getopts "d:p:" OPTION
do
	case $OPTION in
	d)
		DOMAIN_ID=$OPTARG
		;;
	p)
		PORT_NUMBER=$OPTARG
		;;
	esac
done

if [ -z "$DOMAIN_ID" ] || [ -z "$PORT_NUMBER" ]; then
	echo "Missing some options; call as ./port-auditor.sh -d DOMAIN_ID -p PORT_NUMBER"
	exit 1
fi

DOMAIN_JSON=`aws route53 list-resource-record-sets --hosted-zone-id $DOMAIN_ID | jq -r '.ResourceRecordSets[].Name' | sed 's/\.$//g' | sort | uniq`

saveIFS=$IFS
IFS=$'\n'
DOMAIN_URI=($DOMAIN_JSON)
IFS=$saveIFS
DOMAIN_COUNT=${#DOMAIN_URI[@]}

SUCCESSFULLY_CONNECTED=0
for ((i=0; i<${#DOMAIN_URI[@]}-1; i+=1))
do
	TEST_URI=`nc -z -w5 ${DOMAIN_URI[$i]} $PORT_NUMBER &> /dev/null; echo $?`
	if [[ $TEST_URI == 0 ]]; then
	echo "SUCCESS connecting to ${DOMAIN_URI[$i]}:$PORT_NUMBER"
	SUCCESSFULLY_CONNECTED=$((SUCCESSFULLY_CONNECTED+1))
	else
	echo "FAILED connecting to ${DOMAIN_URI[$i]}:$PORT_NUMBER"
	fi
done

echo "Succeeded in connecting to $SUCCESSFULLY_CONNECTED/$DOMAIN_COUNT DNS addresses on domain $DOMAIN_ID."